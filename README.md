
<!-- README.md is generated from README.Rmd. Please edit that file -->

# mesoutils

<!-- badges: start -->
<!-- badges: end -->

L’objectif de mes outils est de vous faciliter le travail pour avoir des
informations sur des jeux de donnees et pour faire la somme des colonnes
numeriques

## Installation

You can install the development version of mesoutils like so:

``` r
# library(mesoutils)
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(mesoutils)
## basic example code
get_info_data(data = iris)
#> $dimension
#> [1] 150   5
#> 
#> $names
#> [1] "Sepal.Length" "Sepal.Width"  "Petal.Length" "Petal.Width"  "Species"
get_mean_data(data = iris)
#>   Sepal.Length Sepal.Width Petal.Length Petal.Width
#> 1     5.843333    3.057333        3.758    1.199333
```

What is special about using `README.Rmd` instead of just `README.md`?
You can include R chunks like so:

``` r
summary(cars)
#>      speed           dist       
#>  Min.   : 4.0   Min.   :  2.00  
#>  1st Qu.:12.0   1st Qu.: 26.00  
#>  Median :15.0   Median : 36.00  
#>  Mean   :15.4   Mean   : 42.98  
#>  3rd Qu.:19.0   3rd Qu.: 56.00  
#>  Max.   :25.0   Max.   :120.00
```

You’ll still need to render `README.Rmd` regularly, to keep `README.md`
up-to-date. `devtools::build_readme()` is handy for this. You could also
use GitHub Actions to re-render `README.Rmd` every time you push. An
example workflow can be found here:
<https://github.com/r-lib/actions/tree/v1/examples>.

In that case, don’t forget to commit and push the resulting figure
files, so they display on GitHub and CRAN.
